import 'package:flutter/material.dart';
import 'package:flutter_pet/search_books_example/models.dart';

class BookScreen extends StatelessWidget {
  final Book _book;

  BookScreen(this._book);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text(_book.title)),
        body: SizedBox.expand(
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 24.0),
                child:
                    _book.url != null ? Image.network(_book.url) : Container(),
              ),
              Padding(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 16.0),
                child: Text(
                  _book.title,
                  maxLines: 10,
                ),
              ),
            ],
          ),
        ));
  }
}
