import 'package:flutter_pet/search_books_example/models.dart';
import 'package:flutter_pet/search_books_example/repository/repository.dart';
import 'package:rxdart/rxdart.dart';

class SearchBooksViewModel {
  final Repository _repository;
  final _viewState = BehaviorSubject<SearchBooksViewState>(
      seedValue: SearchBooksViewState.initState());
  final _events = PublishSubject<SearchBooksEvents>();

  Stream<SearchBooksViewState> get viewState => _viewState.stream;

  Sink<SearchBooksEvents> get events => _events.sink;

  SearchBooksViewModel(this._repository) {
    final searchStringChanged = _events
        .where((event) => event is SearchStringChanged)
        .map((event) => (event as SearchStringChanged).searchString)
        .debounce(Duration(milliseconds: 300))
        .flatMap((searchString) {
      final startWith = (SearchBooksViewState currentState) {
        final newState = currentState;
        newState.isLoading = true;
        return currentState;
      };
      final onErrorReturnWith = (e) => (SearchBooksViewState currentState) {
            final newState = currentState;
            if (e is Exception) newState.exception = e;
            newState.isLoading = false;
            return currentState;
          };
      final books = (bookList) => (SearchBooksViewState currentState) {
            final newState = currentState;
            newState.items = bookList;
            newState.isLoading = false;
            newState.exception = null;

            return currentState;
          };

      return Observable.fromFuture(
              _repository.fetchPopularBooksList(searchString))
          .map(books)
          .startWith(startWith)
          .onErrorReturnWith(onErrorReturnWith);
    });

    final stateReducers = [searchStringChanged];

    Observable.merge(stateReducers)
        .scan((currentState, stateReduser, _) => stateReduser(currentState),
            SearchBooksViewState.initState())
        .listen((newState) {
      _viewState.add(newState);
    });

    final a = Book("", "");

    _viewState.listen(print);
  }

  dispose() {
    _viewState.close();
    _events.close();
  }
}

class SearchBooksViewState {
  List<Book> items = new List();
  bool isLoading = false;
  Exception exception;

  get isException => exception != null && !isLoading;

  SearchBooksViewState.initState() {
    items = new List();
    isLoading = false;
    exception = null;
  }

  @override
  String toString() {
    return 'SearchBooksViewState{items: ${items.length}, isLoading: $isLoading, exception: $exception}';
  }
}

class SearchBooksEvents {}

class SearchStringChanged implements SearchBooksEvents {
  String searchString;

  SearchStringChanged(String searchString) {
    this.searchString = searchString;
  }
}
