import 'package:flutter/material.dart';
import 'package:flutter_pet/search_books_example/features/book/book_screen.dart';
import 'package:flutter_pet/search_books_example/features/search_books/search_books_view_model.dart';
import 'package:flutter_pet/search_books_example/main.dart';

class SearchBooksScreen extends StatefulWidget {
  final String title;

  SearchBooksScreen({Key key, this.title}) : super(key: key);

  @override
  State createState() =>
      _SearchBooksScreenState(container<SearchBooksViewModel>());
}

class _SearchBooksScreenState extends State<SearchBooksScreen> {
  final SearchBooksViewModel _searchBooksViewModel;

  _SearchBooksScreenState(this._searchBooksViewModel);

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _searchBooksViewModel.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: StreamBuilder(
        stream: _searchBooksViewModel.viewState,
        builder: (context, AsyncSnapshot<SearchBooksViewState> snapshot) {
          if (snapshot.hasData) {
            final items = snapshot.data.items;
            final isLoading = snapshot.data.isLoading;
            final isException = snapshot.data.isException;

            Widget booksContainer;

            if (isException) {
              booksContainer = Container(
                padding: EdgeInsets.all(16.0),
                child: Text("Произошла ошибка."),
              );
            } else if (isLoading) {
              booksContainer = Container(
                padding: EdgeInsets.all(16.0),
                child: CircularProgressIndicator(),
              );
            } else {
              booksContainer = Flexible(
                  child: ListView.builder(
                      itemCount: items.length,
                      itemBuilder: (BuildContext context, int index) {
                        return GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        BookScreen(items[index])));
                          },
                          child: Card(
                            child: Padding(
                              padding: EdgeInsets.all(8.0),
                              child: Row(
                                children: <Widget>[
                                  items[index].url != null
                                      ? Image.network(items[index].url)
                                      : Container(),
                                  Flexible(
                                    child: Container(
                                      margin: EdgeInsets.only(left: 8.0),
                                      child: Text(
                                        items[index].title,
                                        maxLines: 10,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      }));
            }

            return Container(
              padding: EdgeInsets.all(16.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  TextField(
                    decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.black, width: 0.0)),
                        border: OutlineInputBorder(),
                        labelText: "Найти книгу",
                        labelStyle: TextStyle(color: Colors.black)),
                    onChanged: (searchString) => _searchBooksViewModel.events
                        .add(SearchStringChanged(searchString)),
                  ),
                  booksContainer
                ],
              ),
            );
          }

          return Container();
        },
      ),
    );
  }
}
