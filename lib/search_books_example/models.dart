class Book {
  String title, url;

  Book(String title, String url) {
    this.title = title;
    this.url = url;
  }

  @override
  String toString() {
    return 'Book{title: $title, url: $url}';
  }
}
