import 'dart:async';
import 'dart:convert';

import 'package:flutter_pet/search_books_example/models.dart';
import 'package:http/http.dart' show Client;

class GoogleBooksApiProvider {
  Client client = Client();
  final _baseUrl = "https://www.googleapis.com/books/v1/volumes";

  Future<List<Book>> fetchPopularBooksListList(String booksName) async {
    final response = await client.get("$_baseUrl?q=$booksName");
    if (response.statusCode == 200) {
      final books =
          (json.decode(response.body)['items'] as List).map((googleBook) {
        final title = googleBook["volumeInfo"]["title"];
        final url = googleBook["volumeInfo"]["imageLinks"]["smallThumbnail"];
        return Book(title, url);
      }).toList();

      return books;
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to load post');
    }
  }
}