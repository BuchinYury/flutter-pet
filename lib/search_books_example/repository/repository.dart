import 'dart:async';

import 'package:flutter_pet/search_books_example/models.dart';
import 'package:flutter_pet/search_books_example/repository/google_books_api_provider.dart';

class Repository {
  final moviesApiProvider = GoogleBooksApiProvider();

  Future<List<Book>> fetchPopularBooksList(String booksName) =>
      moviesApiProvider.fetchPopularBooksListList(booksName);
}
