import 'package:flutter/material.dart';
import 'package:flutter_pet/search_books_example/app.dart';
import 'package:flutter_pet/search_books_example/features/search_books/search_books_view_model.dart';
import 'package:flutter_pet/search_books_example/repository/repository.dart';
import 'package:kiwi/kiwi.dart' as kiwi;

kiwi.Container container = kiwi.Container();

void main() {
  initDependencyInjection();
  runApp(BooksApp());
}

void initDependencyInjection() {
  container.registerInstance(Repository());
  container.registerInstance(SearchBooksViewModel(container<Repository>()));
}