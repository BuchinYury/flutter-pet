import 'package:flutter/material.dart';
import 'package:flutter_pet/search_books_example/features/search_books/search_books_screen.dart';

class BooksApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      showPerformanceOverlay: false,
      title: 'Book Shelf',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: SearchBooksScreen(title: 'Book Shelf'),
    );
  }
}
